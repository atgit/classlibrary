﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
enum EmployeeType {Direct=0,Indirect=1,Manger=2 }
enum WorkType {Temporary=0, Permanent=1,HA=2 }


namespace ClassLibrary
{
    class Employee
    {
        public int Employee_Num { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public DateTime Date_Start { get; set; }
        public DateTime Stop_Work { get; set; }
        public string Id { get; set; } //ת.ז
        public EmployeeType Employee_Type { get; set; }//סוג עובד ישיר,עקיף,מנהל
        public DateTime Employee_Birthday { get; set; }
        public WorkType workType { get; set; }//קבוע/זמני/ח.א
        public string English_First_Name { get; set; }
        public string English_Last_Name { get; set; }
        public int seniority { get; set; } //ותק
        public bool Status { get; set; }//פעיל/לא פעיל
        public int Unit { get; set; }//מחלקה
        string StrSql = "";

        public Employee()
        {
          
        }

        public static List<Employee> GetListOfActiveEmployees()
        {
            DBService DBS = new DBService();
            DataTable DataTable = new DataTable();
            string Employee_Table = $@"SELECT  T.OVED as Number ,L.PRATI as FirstName ,L.FAMILY as LastName,T.mifal,
                                     L.AVODA as DateStart, T.SUGOVD as TypeTime, L.MAHLAKA as Unit,T.BIRTHDAY 
                                    FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED 
                                    WHERE T.mifal='01'";
            DataTable = DBS.executeSelectQueryNoParam(Employee_Table);
            List<Employee> toReturn = new List<Employee>();
            foreach(DataRow row in DataTable.Rows)
            {
                
               Employee temp= new Employee {Employee_Num=int.Parse(row["NUMBER"].ToString()),First_Name=row["FIRSTNAME"].ToString(),
                                           Last_Name=row["LASTNAME"].ToString(),
                                           workType=(WorkType)int.Parse(row["TYPETIME"].ToString()),Unit=int.Parse(row["UNIT"].ToString())
                                           
                };
                if ((row["DATESTART"].ToString() != "000000"))
                    temp.Date_Start = DateTime.ParseExact(row["DATESTART"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
                else
                {
                    temp.Date_Start = DateTime.MinValue;
                }
                if ((row["BIRTHDAY"].ToString() != "000000"))
                    temp.Employee_Birthday = DateTime.ParseExact(row["BIRTHDAY"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
                else
                {
                    temp.Employee_Birthday = DateTime.MinValue;
                }
                toReturn.Add(temp);
            }
            return toReturn;
        }
        public bool CheckIfHaveBdayInRangeOfMonths(DateTime from, DateTime to)
        {
            return (this.Employee_Birthday.Date.Month > from.Date.Month || (this.Employee_Birthday.Date.Month == from.Date.Month && this.Employee_Birthday.Date.Day >= from.Date.Day))
                      &&
                      (this.Employee_Birthday.Date.Month < to.Date.Month || (this.Employee_Birthday.Month == to.Date.Month && this.Employee_Birthday.Date.Day <= to.Date.Day));
        }

        public DataTable GetData()
        {
            DBService DBS = new DBService();
            DataTable dataTable = new DataTable();
            string Employee_Table = $@"SELECT  T.OVED as Number ,L.PRATI as FirstName ,L.FAMILY as LastName,
                                     L.AVODA as DateStart, T.SUGOVD as TypeTime, L.MAHLAKA as Unit,T.BIRTHDAY 
                                    FROM isufkv.isav as T left join isufkv.isavl10 as L on T.OVED=L.OVED 
                                    WHERE T.mifal='01'";
            try
            {
                DateTime bday = DateTime.MinValue;
                bday = DateTime.ParseExact(DBS.executeSelectQueryNoParam(Employee_Table).Rows[0]["BIRTHDAY"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception EX) { }
            dataTable = DBS.executeSelectQueryNoParam(Employee_Table);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i]["BIRTHDAY"].ToString() != "000000")
                {
                    dataTable.Rows[i]["DateStart"] = DateTime.ParseExact(dataTable.Rows[i]["DateStart"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
                    dataTable.Rows[i]["BIRTHDAY"] = DateTime.ParseExact(dataTable.Rows[i]["BIRTHDAY"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture);
                }
            }

            return dataTable;
            //DateTime.ParseExact(dbAS400.executeSelectQueryNoParam(StrSqlAS400).Rows[0]["BIRTHDAY"].ToString(), "ddMMyy", System.Globalization.CultureInfo.InvariantCulture)
        }


        public Employee(string id)
        {
            Id = id;
        }

        public Employee(string id, int workernum, string name, string lastname, DateTime birthday)
        {
            Id = id;
            Employee_Num = workernum;
            First_Name = name;
            Last_Name = lastname;
            Employee_Birthday = birthday;
        }

        public void GetPersonDetails()
        {
            DataTable DT = new DataTable();
            DbServiceSQL_Prod dbs = new DbServiceSQL_Prod();
            StrSql = $@"SELECT * 
                        FROM HR   
                        WHERE ID='{Id}'";
            DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text).Tables[0];
            if (DT.Rows.Count > 0)
            {
                Id = DT.Rows[0]["ID"].ToString();
                Employee_Num = int.Parse(DT.Rows[0]["ID"].ToString());
                First_Name = DT.Rows[0]["ID"].ToString();
                Last_Name = DT.Rows[0]["ID"].ToString();
                Employee_Birthday = DateTime.Parse(DT.Rows[0]["ID"].ToString());
            }
        }

        public void GetPersonDetails(DataRow row)
        {
            Id = row["ID"].ToString();
            Employee_Num = int.Parse(row["ID"].ToString());
            First_Name = row["ID"].ToString();
            Last_Name = row["ID"].ToString();
            Employee_Birthday = DateTime.Parse(row["ID"].ToString());
        }

        public static DataTable GetBirthEmp()
        {
            DbServiceSQL_Prod dbs = new DbServiceSQL_Prod();
            DataTable DT = new DataTable();
            // שליפת כלל העובדים שיש להם היום יום הולדת
            string StrSql = $@"SELECT * 
                              FROM HR   
                              WHERE MONTH(Date_Of_Birth) = '{DateTime.Today.Month}' AND DAY(Date_Of_Birth) = '{DateTime.Today.Day}' AND Active = 1";
            return DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text).Tables[0];
        }
    }
}
